(function (app) {
    var showWelcomeMsg = false;
    var img_dir = "assets/img/";
    app.HomeView = app.View.extend({
        initialize: function () {
            //only create template once, but ensure that creation takes place after app is loaded
            if (!this.template) app.HomeView.prototype.template = this.template || _.template($('#home-template').html());

            this.apps = [];
            this.apps.push({
                title: "Smile",
                id: "smile",
                image: img_dir + "smile_grey.png",
                description: "SMILE flips a traditional classroom into a highly interactive learning environment by engaging learners in critical reasoning and problem solving while enabling them to generate, share, and evaluate multimedia-rich inquiries."
            });
            this.apps.push({
                title: "Wikipedia",
                id: "wikipedia",
                image: img_dir + "wikipedia.png",
                description: "Wikipedia is the world's largest collaborative encyclopedia. The complete collection of over 4.5 million articles from the English-language Wikipedia online encyclopedia (no images)."
            });
            this.apps.push({
                title: "Khan Academy",
                id: "khan",
                image: img_dir + "khan_academy.png",
                description: "Thousands of Khan Academy blackboard-style videos on Math and Sciences organized by subject, selected from the Rachel PI library."
            });
            this.apps.push({
                title: "Project Gutenberg",
                id: "gutenberg",
                image: img_dir + "gutenberg.png",
                description: "Thousands of books in digital format (text) from Project Gutenberg. Organized by Bookshelves."
            });
            this.apps.push({
                title: "CK12",
                id: "ck12",
                image: img_dir + "ck12.png",
                description: "High quality and curated Textbooks collection on STEM (Science, Technology, Engineering and Math) from ck12.org. PDF format."
            });
            this.apps.push({
                title: "USB Files",
                id: "usb",
                hash: "usb",
                image: img_dir + "usb.png",
                description: "USB Files on thumb-drives connected to the SMILE Plug."
            });
        },

        events: {
            'click .close': 'closeButton',
            'click .resource-btn': 'buttonPress',
            'click .resource-img img': 'buttonPress'
        },

        closeButton: function(ev) {
            var target = ev.target;
            var parent = target.parentElement.parentElement;
            logger.info("closed: " + parent.className);
            $(parent).remove();
            showWelcomeMsg = false;
        },

        buttonPress: function(ev) {
            console.log(ev);
            var id = ev.target.getAttribute("data-app-id");
            console.log(id);
            if (id == "smile") {
                logger.info("navigating to app smile");
                app.showLoadingModal({ "text": "Loading SMILE at " + window.location.origin + "/smileweb" });
                window.location = "/smileweb";
            } else if (id == "wikipedia") {
                logger.info("navigating to wikipeda");
                app.showLoadingModal({ "text": "Loading Wikipedia at " + window.location.origin + ":8010/wikipedia_en_all_nopic_01_2012/" });
                window.location =  window.location.origin + ":8010/wikipedia_en_all_nopic_01_2012/";
            } else if (id == "khan") {
                logger.info("navigating to gutenberg");
                window.location =  window.location.origin + "/khan/";
            } else if (id == "gutenberg") {
                logger.info("navigating to gutenberg");
                window.location =  window.location.origin + "/gutenberg/";
            } else if (id == "ck12") {
                logger.info("navigating to ck12");
                window.location =  window.location.origin + "/ck12/";
            } else if (id == "usb") {
                logger.info("navigating to app files");
                window.location.hash = "#usb";
            }
        },

        render: function (options) {
            //apps are defined in init
            this.$el.html(this.template({ showWelcomeMsg: showWelcomeMsg, apps: this.apps } ));
            var that = this;
            logger.info("HomeView rendered");
            return this;
        },

        remove: function () {
            logger.info("HomeView removed");
            this.parentRemove();
        }
    })
})(app);